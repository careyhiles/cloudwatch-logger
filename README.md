CloudWatch Logger
=========================================
Simple Logger for Creation's F3-based CMS

Usage
```
$logger = \CareyHiles\CloudWatchLogger\Logger::getInstance();
$logger->setLogName(Config::get('LOG_NAME')); //prepend with file:// for file logging
$logger->setLogLevel($logger::INFO); //Defaults to INFO

$logger::set('STRIP_NAMESPACE', 'Creation\\'); //optionally strip common namespaces
$logger::set('AWS_CREDENTIALS', self::get_aws_creds()); //Guzzle Promise format

$logger::set('LOG_STREAM', self::get('ENVIRONMENT')); //LOG_STREAM required for CW

//Send some logs'
        \CareyHiles\CloudWatchLogger\Logger::debug('This is simple a debugging message', ['debug' => true]);
        \CareyHiles\CloudWatchLogger\Logger::info('Hello, I\'m a logger, that is some information for you');
        \CareyHiles\CloudWatchLogger\Logger::notice('Take note!', ['key_1'=>'val_1', 'key_2' =>['foo' => 'bar', 'baz' => 'quz'], 'key_3'=>1234]);
        \CareyHiles\CloudWatchLogger\Logger::warning('This IS A WARNING');
        \CareyHiles\CloudWatchLogger\Logger::error('Opps. There is an error!');
        \CareyHiles\CloudWatchLogger\Logger::critical('CRITICAL ERROR');
        \CareyHiles\CloudWatchLogger\Logger::alert('WOOP WOO THIS IS AN ALERT');
        \CareyHiles\CloudWatchLogger\Logger::emergency('Emergency! There\'s an emergency happening');
        //\CareyHiles\CloudWatchLogger\Logger::abort('Just stop it'); //abort level will exit the appliction with the message;

die('test done');
```